/*****************************************************************************
@Name:  public class ServiceLogDelBatchTest{

@=========================================================================
@Purpose: Test class for the SchedularAccountExperyEmail
                                                                                                   
@=========================================================================
@History                                                            
@---------                                                            
@VERSION   AUTHOR                            DATE                DETAIL                                 
@1.0 -    Sachin Patkar                  12/05/2016        INITIAL DEVELOPMENT               
 ******************************************************************************/

@isTest
public class ServiceLogDelBatchTest{

     static testmethod void test() {
             Service_Log__c servcRec = new Service_Log__c();
             servcRec.CurrencyIsoCode = 'INR';
             Insert servcRec ;
             
             ServiceLogDelBatchClass serviceLogBatch = new ServiceLogDelBatchClass();
             ID batchid = Database.executeBatch(serviceLogBatch);    
     
     }

}