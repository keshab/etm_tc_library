@isTest
public class SchedularServiceLogDelTest{
    static testmethod void test() {
         Service_Log__c servcRec = new Service_Log__c();
         servcRec.CurrencyIsoCode = 'INR';
         Insert servcRec ;
      
        
         Test.startTest();
             SchedularServiceLogDelBatchClass accExpEmail = new SchedularServiceLogDelBatchClass();  
             //database.executeBatch(accExpEmail );
             String chron = '0 0 23 * * ?';        
            system.schedule('Test Sched', chron, accExpEmail );
         Test.stopTest();       
    }
}