/*






*/


public class GroupUser{
public static    set<ID> groupIds=new set<ID> ();
public static  Map<id,List<groupMember>> getQueueMember( set<id> queueIds){

    List<Group> allGroup= new List<Group>();
    Map<id,Group> allGroupinSystemMap= new Map<id,Group>() ;
    List<groupMember> allUser= new List<groupMember>();
    List<groupMember> allgroupMember= new List<groupMember>();
    Map<id,List<groupMember>> QandMember=new Map<id,List<groupMember>> ();
    
    Map<id,List<groupMember>> QandMemberForALL=new Map<id,List<groupMember>> ();
        allGroupinSystemMap=new Map<id,Group>([Select g.Type, g.Name,g.Id, g.Email, g.DeveloperName,(Select Id, GroupId, UserOrGroupId,group.Id,group.Name,SystemModstamp From GroupMembers)  From Group G where ( type='queue' or  type='Regular') ]);
    
    for(Group grrp: allGroupinSystemMap.values()){
        List<GroupMember> groupList=new List<GroupMember> ();
        groupList=grrp.getSobjects('GroupMembers');
        List<GroupMember> groupListforMap=new List<GroupMember> ();
        IF(groupList<>null){
            groupListforMap=QandMemberForALL.get(grrp.id);
            if(groupListforMap<>null){
                groupListforMap.addall(groupList);
                QandMemberForALL.put(grrp.id,groupListforMap);
            }else if(groupListforMap==null){
            groupListforMap=new List<GroupMember> ();
                groupListforMap.addall(groupList);
                QandMemberForALL.put(grrp.id,groupListforMap);
            }
        }
    
    }
    List<Group> QueueList=new List<Group>();
    if(queueIds<>null && queueIds.size()>0){
       QueueList=[Select g.Type, g.Name,g.Id, g.Email, g.DeveloperName From Group g WHERE type='Queue' and id in : queueIds];
    }else{
       QueueList=[Select g.Type, g.Name,g.Id, g.Email, g.DeveloperName From Group g WHERE type='Queue' ];
    }
    system.debug('================QueueList'+QueueList); 
    for(group grp: QueueList){
        QandMember.put(gRP.Id,new list<groupMember>());
    }
    for(id ids :QandMember.keyset()){
        system.debug(ids);
        groupIds=new set<ID> ();
        groupIds.add(ids);
        while(groupIds<>null && groupIds.size()>0){
            system.debug(groupIds);
            allUser= new List<groupMember>();
            allUser=findUser(groupIds,QandMemberForALL );
            system.debug('================allUser'+allUser); 
            if(allUser<>null){
                allUser.addAll(QandMember.get(ids));
                QandMember.put(ids,allUser);
            
            }
        }
     }
    system.debug('================'+QandMember); 
return QandMember;
}


public static  list<groupMember> findUser( set<id> grpIds,Map<Id,List<GroupMember>> allGroupinMap){
    system.debug('===========grpIds====='+grpIds);  
    system.debug(allGroupinMap.values());
    system.debug(grpIds);   
    list<groupMember>  grpMem=new list<groupMember>();
    groupIds=new set<ID> ();
    for(id ids : grpIds){
        List<groupMember> grp=allGroupinMap.get(ids);
        if(grp<>null){
        //system.debug('==========grp======'+grp);
            //list<groupMember> aalchild=grp.getSobjects('groupMembers');
           // system.debug('==========aalchild======'+grp);
            if(grp<>null){
            for(groupMember gm : grp){
                if(String.valueOf(gm.UserOrGroupId).startsWith('005')){
                 system.debug('==========aalchild======'+gm.UserOrGroupId);
                    grpMem.add(gm);
                }else {
                    groupIds.add(gm.UserOrGroupId);    
                }
            }
            }
        }
    }
    
    return grpMem;
}



     public static  Map<id,List<Group>>  getUserAndQueuesMap(Map<id,List<groupMember>> queueUserMap){
        MAP<id,Group>    QueueMap=new MAP<id,Group> ([Select g.Type, g.Name,g.Id, g.Email, g.DeveloperName From Group g WHERE type='Queue']);

         Map<ID,List<Group>> mapofIdGroupMap = new Map<ID,List<Group>>();
         Map<ID,set<id>> mapofIdGroupList = new Map<ID,set<id>>();
         List<GroupMember> groupMembers = new List<GroupMember>();
         List<Group> groupList=new List<Group>();
         set<Id> groupset=new  set<Id> (); 

         Group g;
         for(Id Qids:queueUserMap.keySet()){
            groupMembers = queueUserMap.get(Qids);
            for(GroupMember grpMbr: groupMembers){
                groupset=new set<Id> ();
                groupset=mapofIdGroupList.get(grpMbr.UserOrGroupId);
                if(groupset<>null){
                    
                    groupset.add(Qids);
                     mapofIdGroupList.put(grpMbr.UserOrGroupId,groupset)    ;               
                }else{
                     groupset=new set<Id> ();
                     groupset.add(Qids);
                     mapofIdGroupList.put(grpMbr.UserOrGroupId,groupset)       ;               
                    
                }
                
            }
         }
          for(Id Userids: mapofIdGroupList.keySet()){
            set<id> QIds =mapofIdGroupList.get(Userids);
            groupList=new List<Group>();
            for(id qid: QIds){
                groupList.add(QueueMap.get(qid));               
            }
            mapofIdGroupMap.put(Userids,groupList);              
             
         }
         return mapofIdGroupMap;
    
    }

}