/*
*
*Author: Pavithra Gajendra
*Description: Utility Class
*Created Date: 27/01/16
*Version: 1.0
*
*/ 
public class CT_Utility {
    
    /*****************************************
    Purpose     : Method for getting Record Type Id                                  
    Return Type : Record Id
    *****************************************/
    public static Id getRecordTypeId(String objectType,String recordTypeName){
                
        Id recTypeId = Schema.getGlobalDescribe().get(objectType).getDescribe().getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        return recTypeId ;      
    }
    
     /****************************************************
    Purpose     : Get List of String in Lower Case            
    Return Type : String
    *****************************************************/
    public static Set<String> getStringInLoweCase(String stringList){
        
        Set<String> returnList = new Set<String>();
        for(String s : stringList.split(',')){
            returnList.add(s.toLowerCase());
        }        
        return returnList;
    } 
       
   /*****************************************
    Purpose     : Method for Sending Email                                  
    Return Type : Nothing
    *****************************************/
    public static void sendEmailMethod(List<String> emailList,String emailBody,String subject){
        
        Messaging.SingleEmailMessage mailHandler = new Messaging.SingleEmailMessage();
        mailHandler.setToAddresses(emailList);
        mailHandler.setSenderDisplayName(userInfo.getName());
        mailHandler.setSubject(subject);
        mailHandler.setHtmlBody(emailBody);
        
        try{
            Messaging.sendEmail(new Messaging.Email[] { mailHandler });
        }catch(EmailException ex){
            System.debug(ex.getMessage());
        }
    }
    
    /*****************************************
    Purpose     : Method to get profileId                                  
    Return Type : Record Id
    *****************************************/
    public static Id getProfileId(String profileName){
        
        return [Select Id FROM Profile Where Name=:profileName].Id ;
    }
    
    
    /*****************************************************************************
    @Name:  checkDisabledTriggerEvent     
    @=========================================================================
    @Purpose: This method is used to active and deactivate trigger                                                                                                           
    @=========================================================================
    @History                                                            
    @---------                                                            
    @VERSION AUTHOR                            DATE                DETAIL                                 
    @1.0 - Dhriti Krishna Ghosh Moulick      29-01-2016         INITIAL DEVELOPMENT               
     ******************************************************************************/
    public static boolean checkDisabledTriggerEvent(String TriggerControllerName,String event) { 
        boolean isDisabled=false;
        try{   
           Trigger_Controller__c triggerController =Trigger_Controller__c.getValues(TriggerControllerName);   
            if(triggerController<>null && triggerController.IsActive__c==true){
                if(triggerController.get(event)!=true){
                 isDisabled=true;
                }     
            }
           return isDisabled;
        }catch(Exception e){
           return false;
        }
    }
    
        /******************************************************************* 
            Purpose:  Creating this method to form a string combination of Airline and booking type                                                       
            ---------------------------------------------------                               
            VERSION AUTHOR                              DATE            DETAIL                                 
            1.0 -Dhriti Krishna Ghosh Moulick         20/01/2016      INITIAL DEVELOPMENT   
                            
        *********************************************************************/ 
        public static String getKey(String booking,String Airline ){
            String key;
                if(Airline!=null && booking!=null){
                  key = Airline+'_'+booking;
                   //useIDAirBookValue.put(userId,key);
                }else if(Airline!=null && booking==null){
                   key = Airline+'_';
                  // useIDAirBookValue.put(userId,key);
                }else if(Airline==null && booking!=null){
                   key = '_'+booking;
                   //useIDAirBookValue.put(userId,key);
                }else if(Airline==null && booking==null){
                   key = '__';
                  // useIDAirBookValue.put(userId,key);
                }               
        
            return key;
        }
        /******************************************************************* 
            Purpose:  Creating this method to form a string combination of Airline                                                       
            ---------------------------------------------------                               
            VERSION AUTHOR                              DATE            DETAIL                                 
            1.0 -Dhriti Krishna Ghosh Moulick         11/04/2016      INITIAL DEVELOPMENT   
                            
        *********************************************************************/ 
        public static String getKey1(String booking,String Airline ){
            String key;
               if(Airline!=null){
                   key = Airline+'_';
               }
            return key;
        }
        /******************************************************************* 
            Purpose:  Creating this method to form a string combination of  booking type                                                       
            ---------------------------------------------------                               
            VERSION AUTHOR                              DATE            DETAIL                                 
            1.0 -Dhriti Krishna Ghosh Moulick         11/04/2016      INITIAL DEVELOPMENT   
                            
        *********************************************************************/ 
         public static String getKey2(String booking,String Airline ){
            String key;
              if(booking!=null){
                   key = '_'+booking;
               }
            return key;
        }
        
        /*******************************************************************
            Purpose:  This method is used to return Map of String and List of ID(Key is combination of Airline_Booking type and value is list of Users)                                                        
            ---------------------------------------------------                               
            VERSION AUTHOR                              DATE            DETAIL                                 
            1.0 -Dhriti Krishna Ghosh Moulick         20/01/2016      INITIAL DEVELOPMENT   
                            
        *********************************************************************/ 
         public static Map<String,List<id>> setMapOfkeyUser(String Key,Id value,Map<String,List<id>> mapKId ){
            List<id> userIdList=new List<id>();
                userIdList=mapKId.get(key);
                if(userIdList<>null && userIdList.size()>0){
                    userIdList.add(value);
                    mapKId.put(Key,userIdList);
                }else if(userIdList==null ){
                    userIdList=new List<Id>();
                    userIdList.add(value);
                    mapKId.put(Key,userIdList); 
                }
            
            return mapKId;
        }
        
        /*******************************************************************
            Purpose:  This method is used to return Map of String and List of Case(Key is combination of Airline_Booking type and value is list of Cases)                                                        
            ---------------------------------------------------                               
            VERSION AUTHOR                              DATE            DETAIL                                 
            1.0 -Dhriti Krishna Ghosh Moulick         21/01/2016      INITIAL DEVELOPMENT   
                            
        *********************************************************************/ 
        public static Map<String,List<Case>> setMapOfKeyCase(String Key,Case value,Map<String,List<Case>> finalCaseMap){
                List<case> caseForMapList=new List<case>();
                if(Key=='__'){

                    caseForMapList=finalCaseMap.get('__');
                    if(caseForMapList==null){
                    caseForMapList=new List<case>();
                    }  
                    caseForMapList.add (value);
                    finalCaseMap.put(key,caseForMapList);
                }
                if(key<>null&& Key<>'__'){
                    caseForMapList=finalCaseMap.get(key); 
                    if(caseForMapList<>null ){
                        caseForMapList.add(value);
                        finalCaseMap.put(key,caseForMapList);
                    }else if(caseForMapList==null ){
                        caseForMapList=new List<Case>();
                        caseForMapList.add(value);
                        finalCaseMap.put(key,caseForMapList);
                    }
                }
                return finalCaseMap;
        }
          /*******************************************************************
            Purpose:  This method is used to return case status based on Team Status and Queue Name                                                      
            ---------------------------------------------------                               
            VERSION AUTHOR                              DATE            DETAIL                                 
            1.0 -Dhriti Krishna Ghosh Moulick         16/02/2016      INITIAL DEVELOPMENT   
                            
        *********************************************************************/ 
        public static String  fetchCaseStatus(String teamStatus,String currentQueueName,Boolean isReopen,String cseReason){
            String caseStatus;
           /* if(currentQueueName == CT_Constants.AFF_SHELVED_QUEUE){
                if(cseReason <> null){
                  caseStatus = cseReason;   
                }
            }*/
            if(currentQueueName == CT_Constants.ESCALATION_EMAIL_QUEUE){
                if(teamStatus == CT_Constants.TEAM_AMENDMENT_NOT_DONE || teamStatus == CT_Constants.TEAM_STATUS_CLOSED || teamStatus == CT_Constants.TEAM_STATUS_CANCELLED ||
                   teamStatus == CT_Constants.TEAM_AMENDMENT_DONE_LCC || teamStatus == CT_Constants.TEAM_AMENDMENT_DONE_GDS || teamStatus == CT_Constants.TEAM_AMENDMENT_DONE_Supplier ||
                   teamStatus == CT_Constants.ON_HOLD_REJECT) {
                    caseStatus = CT_Constants.CASE_CLOSED;
                }else{
                    caseStatus = CT_Constants.CASE_ESCALATED;
                }
            }
            else if(isReopen == false){
                if(teamStatus == CT_Constants.TEAM_STATUS_UNASSIGNED || teamStatus == CT_Constants.TEAM_STATUS_ASSIGN || teamStatus == CT_Constants.TEAM_STATUS_IN_PROGRESS || 
                   teamStatus == CT_Constants.TEAM_AMENDMENT_DONE || teamStatus == CT_Constants.TEAM_STATUS_DONE || teamStatus == CT_Constants.TEAM_STATUS_LEVEL_ONE_ESCALATION ||
                   teamStatus == CT_Constants.TEAM_STATUS_LEVEL_TWO_ESCALATION || teamStatus == CT_Constants.TEAM_STATUS_LEVEL_THREE_ESCALATION || teamStatus == CT_Constants.TEAM_STATUS_ALTERNATE ||
                   teamStatus == CT_Constants.TEAM_STATUS_BOUNCED || teamStatus == CT_Constants.TEAM_STATUS_SOLDOUT || teamStatus == CT_Constants.ON_HOLD_CONFIRMED || teamStatus == CT_Constants.TEAM_AMENDMENT_DONE_GDS){
                    caseStatus = CT_Constants.CASE_OPEN;
                }
                else if(teamStatus == CT_Constants.TEAM_STATUS_PENDING_AGENT || teamStatus == CT_Constants.TEAM_STATUS_PENDING_TASKS || 
                        teamStatus == CT_Constants.TEAM_STATUS_PENDING_SUPPLIER || teamStatus == CT_Constants.TEAM_STATUS_PENDING_AIRLINE ||
                        teamStatus == CT_Constants.TEAM_STATUS_PENDING_CUSTOMER || teamStatus == CT_Constants.TEAM_STATUS_PENDING_ON_BD){
                     caseStatus = CT_Constants.CASE_PENDING;
                }else if(teamStatus == CT_Constants.TEAM_AMENDMENT_NOT_DONE || teamStatus == CT_Constants.TEAM_STATUS_CLOSED || teamStatus == CT_Constants.ON_HOLD_REJECT ||
                         teamStatus == CT_Constants.ON_HOLD_LAPSED || teamStatus == CT_Constants.CONFIRMED ||  teamStatus == CT_Constants.TEAM_AMENDMENT_DONE_Supplier ||  teamStatus == CT_Constants.TEAM_AMENDMENT_DONE_LCC ||
                         teamStatus == CT_Constants.TEAM_STATUS_CANCELLED){
                     caseStatus = CT_Constants.CASE_CLOSED;
                }
            }else if(isReopen == true){
                if(teamStatus == CT_Constants.TEAM_STATUS_UNASSIGNED || teamStatus == CT_Constants.TEAM_STATUS_ASSIGN || teamStatus == CT_Constants.TEAM_STATUS_IN_PROGRESS || 
                   teamStatus == CT_Constants.TEAM_AMENDMENT_DONE || teamStatus == CT_Constants.TEAM_STATUS_DONE || teamStatus == CT_Constants.TEAM_STATUS_LEVEL_ONE_ESCALATION ||
                   teamStatus == CT_Constants.TEAM_STATUS_LEVEL_TWO_ESCALATION || teamStatus == CT_Constants.TEAM_STATUS_LEVEL_THREE_ESCALATION ||
                   teamStatus == CT_Constants.TEAM_STATUS_PENDING_AGENT || teamStatus == CT_Constants.TEAM_STATUS_PENDING_TASKS || 
                   teamStatus == CT_Constants.TEAM_STATUS_PENDING_SUPPLIER || teamStatus == CT_Constants.TEAM_STATUS_PENDING_AIRLINE ||teamStatus == CT_Constants.TEAM_STATUS_PENDING_CUSTOMER ||
                   teamStatus == CT_Constants.TEAM_STATUS_ALTERNATE || teamStatus == CT_Constants.TEAM_STATUS_BOUNCED || teamStatus == CT_Constants.TEAM_STATUS_SOLDOUT || teamStatus == CT_Constants.TEAM_STATUS_PENDING_ON_BD){
                    caseStatus = CT_Constants.CASE_REOPENED;
                }else if(teamStatus == CT_Constants.TEAM_AMENDMENT_NOT_DONE || teamStatus == CT_Constants.TEAM_STATUS_CLOSED || teamStatus == CT_Constants.ON_HOLD_REJECT ||
                         teamStatus == CT_Constants.ON_HOLD_LAPSED || teamStatus == CT_Constants.CONFIRMED || teamStatus == CT_Constants.TEAM_AMENDMENT_DONE_Supplier ||  teamStatus == CT_Constants.TEAM_AMENDMENT_DONE_GDS ||
                         teamStatus == CT_Constants.TEAM_AMENDMENT_DONE_LCC || teamStatus == CT_Constants.TEAM_STATUS_CANCELLED){
                    caseStatus = CT_Constants.CASE_CLOSED;
                }
            }
            return caseStatus;
        }
        /*******************************************************************
            Purpose:  This method is used to return Key Prefix of any Sobject                                                      
            ---------------------------------------------------                               
            VERSION AUTHOR                              DATE            DETAIL                                 
            1.0 -Dhriti Krishna Ghosh Moulick         25-02-2016    INITIAL DEVELOPMENT   
                            
        *********************************************************************/ 
        public static String getKeyPrefixSobject(String objectName){
            Schema.DescribeSObjectResult r = Group.sObjectType.getDescribe();
            String keyPrefix = r.getKeyPrefix();
            return keyPrefix;
        }
         public static String getKeyPrefixSobject1(String userName){
            Schema.DescribeSObjectResult r = User.sObjectType.getDescribe();
            String keyPrefix = r.getKeyPrefix();
            return keyPrefix;
        }
    // This method checks if a string is empty or not
    public static Boolean isEmptyString(String str) {
        if(str==null || str.trim().length()==0 || str.equalsIgnoreCase('null')) {
            return true;    
        }
        return false;
    }
    
    public static Set<String> cancellationQueue(){ 
        Set<String> cancellationQueueList = new Set<String>();
        cancellationQueueList.add(CT_Constants.AFF_CANCELLATION_QUEUE);
        //cancellationQueueList.add(CT_Constants.AFF_REFUND_COMPUTATION_QUEUE);
        cancellationQueueList.add(CT_Constants.AFF_REFUND_NOTICE_QUEUE);
        return cancellationQueueList;
    }
    
     /*******************************************************************
            Purpose:  Added function to return Refund Computation Queues                                      
            ---------------------------------------------------                               
            VERSION AUTHOR                              DATE            DETAIL                                 
            1.0 -Dhriti Krishna Ghosh Moulick         25-04-2016      INITIAL DEVELOPMENT   
                            
     *********************************************************************/
    
    public static Set<String> refundComputationQueue(){
    	Set<String> refundComputationQueueList = new Set<String>();
    	refundComputationQueueList.add(CT_Constants.AFF_REFUND_COMPUTATION_QUEUE);
    	return refundComputationQueueList;
    }
    
    public static Set<String> ticketingQueue(){
        
        Set<String> ticketingQueueList = new Set<String>();
        ticketingQueueList.add(CT_Constants.AFF_AMEND_TICKETNG_QUEUE);
        ticketingQueueList.add(CT_Constants.AFF_BOOKING_TICKETING_QUEUE);
        ticketingQueueList.add(CT_Constants.AFF_COUPON_CONVERSION_QUEUE);
        ticketingQueueList.add(CT_Constants.AFF_ENDORSEMENT_EMAIL_QUEUE);
        ticketingQueueList.add(CT_Constants.AFF_OFFLINE_CONVERSION_QUEUE);
        ticketingQueueList.add(CT_Constants.AFF_TICKETING_EMAIL_QUEUE);
        
        return ticketingQueueList;
    }
    //Added by keshab
    
    public static Map<String, String> getRecordTypes(String objType) {
        List<RecordType> rTypes = [SELECT Name, Id FROM RecordType WHERE sObjectType=:objType AND isActive=true];
        //system.debug('rTypes======='+rTypes);
        Map<String, String>listRecordTypes = new Map<String, String>{}; 
        for (RecordType rt:rTypes)    
            listRecordTypes.put(rt.Name, rt.Id);       
            return listRecordTypes;    
    }
    
    public static Map<String,String> returnQueueId(){
    	List<GroupMember> grpmember = [SELECT Group.Id,Group.Name,UserOrGroupId FROM GroupMember WHERE Group.Type = 'Queue' ];
    	Map<String, String> lgroupnameId = new Map<String, String>{}; 
    	for(GroupMember grpMembers:grpmember){
    		lgroupnameId.put(grpMembers.Group.Name,grpMembers.Group.Id);
    	}
    	return lgroupnameId;
    }
    
     
}