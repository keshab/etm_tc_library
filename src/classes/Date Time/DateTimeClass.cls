public class DateTimeClass {

       /**
This method converts string value  dd-mm-yyyy HH:MM:SS or yyyy-mm-dd HH:MM:SS or dd/mm/yyyy HH:MM:SS or yyyy/mm/dd HH:MM:SS  into a dateTime object. 
*/

    public static void convertStringToDateTime(String dateString,String inputformat){
        
        try{
                
        // if input format is provided, returns the datetime in provided format
            if (inputformat != null && dateString != '') {
                try{
                    returnFormatString(dateString,inputformat);   // calling method to return date time in datetime format  
                }catch(Exception e){
                    System.debug(e.getMessage());
                    returnInputFormat(dateString,inputformat);
                }

        }else if(dateString != null && inputformat == null){ // return date time when no format given
					
                    returnDateString(dateString); // calling methodto return date time in datetime format
        }
       

        }
        catch(Exception ex){
        System.debug('---Exception'+ex.getLineNumber()+ex.getMessage());
        
        }
        
    }
    
	// return datetime when format given
    public static Datetime returnFormatString(String dateParam,String changeFormat){

        Datetime conDate;

        if((dateParam != null || dateParam != '') && (changeFormat != null && changeFormat != '')){
            Datetime ds = Datetime.valueOf(dateParam);

            conDate = Datetime.valueOf(ds.format(changeFormat));
        }

        return conDate;
    }

	// return date time when no format given
    public static Datetime returnDateString(String dateParam){

        Datetime conDate;

        if(dateParam != null || dateParam != ''){
                if(dateParam.contains('.')){                    
                    dateParam = dateParam.replace('.','@');
                }else if(dateParam.contains('-')){
                    dateParam = dateParam.replace('-','@');  
                }else if(dateParam.contains('/')){
                    dateParam = dateParam.replace('/','@'); 
                }else if(dateParam.contains(',')){
                    dateParam = dateParam.replace(',','@'); 
                }

                if(dateParam.contains(' ')){
                    dateParam = dateParam.replace(' ','@');
                }

                if(dateParam.contains(':')){
                    dateParam = dateParam.replace(':','@');
                }

                System.debug('inputs--->'+dateParam);
                String[] dateParts = dateParam.split('@');
                System.debug('dateParts--->'+dateParts);

                String year;
                String month;
                String dt;
                String hours = '00';
                String minutes = '00';
                String second = '00';
                String timestatus ='';

                if(dateParts[0] != null && dateParts[0].length() == 4){
                    year = dateParts[0];
                }else if(dateParts[2] != null && dateParts[2].length() == 4){
                    year = dateParts[2];
                }

                if(dateParts[0] != null && dateParts[1] != null && dateParts[0].length()==2 && dateParts[1].length()==2){
                    if(dateParts[0]>dateParts[1] && dateParts[1] <= '12'){
                        month = dateParts[1];
                        dt = dateParts[0];
                    }else if(dateParts[1]>dateParts[0] && dateParts[0] <='12'){
                        month = dateParts[0];
                        dt = dateParts[1];
                    }else{

                    }   
                }else if(dateParts[1] != null && dateParts[2] != null && dateParts[1].length()==2 && dateParts[2].length()==2){
					if(dateParts[1]>dateParts[2] && dateParts[1] <= '12'){
                        month = dateParts[1];
                        dt = dateParts[2];
                    }else if(dateParts[2]>dateParts[1] && dateParts[1] <='12'){
                        month = dateParts[1];
                        dt = dateParts[2];
                    }else{

                    }                       
                }
            
            	if(month.length() == 1){
                    month = '0'+month;
                }
            
            	if(dt.length() == 1){
                	dt = '0'+dt;
            	}

                if(dateParts.size()> 3 && dateParts[3] != null){
                    hours = dateParts[3];
                    if(hours.length() == 1){
                        hours = '0'+hours;
                    }
                }

                if(dateParts.size()>4 && dateParts[4] != null){
                    minutes = dateParts[4];
                    if(minutes.length() == 1){
                        minutes = '0'+minutes;
                    }
                }

                if(dateParts.size()>5 && dateParts[5] != null){
                    second = dateParts[5];
                	if(second.length() == 1){
                        second = '0'+second;
                    }
                }

                if(dateParts.size()>6 && dateParts[6] != null && dateParts[6].contains('AM')){
                    timestatus = dateParts[6];
                }else if(dateParts.size()>6 && dateParts[6] != null && dateParts[6].contains('PM')){
                    timestatus = dateParts[6];
                }


                String fdateString = year + '-' + month    + '-' + dt + ' ' + hours + ':' +  minutes + ':' + second+ ' '+timestatus;
                
                System.debug('dateString----->'+fdateString);

                conDate = Datetime.valueOf(fdateString);
        }

        return conDate;

    }
    
    
    //return datetime when input format is not in salesforce detectable format
    public static Datetime returnInputFormat(String dateString,String inputformat){
		if (inputformat != null && dateString != null) {            

                System.debug('inputformat--->'+inputformat);
                
                if(inputformat.contains('.')){
                    inputformat = inputformat.toLowerCase().replace('.','@');
                    dateString = dateString.toLowerCase().replace('.','@');
                }else if(inputformat.contains('-')){
                    inputformat = inputformat.toLowerCase().replace('-','@');
                    dateString = dateString.toLowerCase().replace('-','@');  
                }else if(inputformat.contains('/')){
                    inputformat = inputformat.toLowerCase().replace('/','@');
                    dateString = dateString.toLowerCase().replace('/','@'); 
                }else if(inputformat.contains(',')){
                    inputformat = inputformat.toLowerCase().replace(',','@');
                    dateString = dateString.toLowerCase().replace(',','@'); 
                }

                if(inputformat.contains(' ')){
                    inputformat = inputformat.toLowerCase().replace(' ','@');
                    dateString = dateString.toLowerCase().replace(' ','@');
                }

                if(inputformat.contains(':')){
                    inputformat = inputformat.toLowerCase().replace(':','@');
                    dateString = dateString.toLowerCase().replace(':','@');

                }
				System.debug('inputs--->'+inputformat);
                String[] inputs = inputformat.split('@');
                String[] dateParts = dateString.split('@');
                System.debug('inputs--->'+inputs);
                System.debug('dateParts--->'+dateParts);

                Integer year;
                Integer month;
                Integer dt;
                Integer hours = 00;
                Integer minutes = 00;
                Integer second = 00;
                String timestatus ='';

                if(inputs[0] != null && inputs[0].toLowerCase().contains('y')){
                    System.debug('in if-----------');
                    year = Integer.valueOf(dateParts[0]);
                }else if(inputs[2] != null && inputs[2].toLowerCase().contains('y')){
                    System.debug('in else-----------');
                    year = Integer.valueOf(dateParts[2]);
                }


                if(inputs[0] != null && inputs[0].toLowerCase().contains('d')){
                    System.debug('in if-----------');
                    dt = Integer.valueOf(dateParts[0]);
                }else if(inputs[1] != null && inputs[1].toLowerCase().contains('d')){
                    System.debug('in else--if--------');
                    dt = Integer.valueOf(dateParts[1]);
                }else if(inputs[2] != null && inputs[2].toLowerCase().contains('d')){
                    System.debug('in else-----------');
                    dt = Integer.valueOf(dateParts[2]);
                }

                if(inputs[0] != null && inputs[0].toLowerCase().contains('m')){
                    month = Integer.valueOf(dateParts[0]);
                }else if(inputs[1] != null && inputs[1].toLowerCase().contains('m')){
                    month = Integer.valueOf(dateParts[1]);
                }else if(inputs[2] != null && inputs[2].toLowerCase().contains('m')){
                    month = Integer.valueOf(dateParts[2]);
                }

                if(inputs.size()>3 && dateParts.size()> 3 && inputs[3] != null && inputs[3].toLowerCase().contains('h')){
                    if(dateParts[3] != null){
                        hours = Integer.valueOf(dateParts[3]);
                    }
                }

                if(inputs.size()>4 && dateParts.size()> 4 && inputs[4] != null && inputs[4].toLowerCase().contains('m')){
                    if(dateParts[4] != null){
                        minutes = Integer.valueOf(dateParts[4]);
                    }
                }

                if(inputs.size()>5 && dateParts.size()> 5 && inputs[5] != null && inputs[5].toLowerCase().contains('s')){
                    if(dateParts[5] != null){
                        second = Integer.valueOf(dateParts[5]);
                    }
                }

                if(inputs.size()>6 && dateParts.size()> 6 && inputs[6] != null && inputs[5].toLowerCase().contains('a')){
                    if(dateParts[6] != null){
                        timestatus = dateParts[6];
                    }
                }


                String fdateString = year + '-' + month    + '-' + dt + ' ' + hours + ':' +  minutes + ':' + second+ ' '+timestatus;
                
                System.debug('dateString----->'+fdateString);

               // Datetime ds = Datetime.parse(dateString);

                Datetime  convertedDate = Datetime.valueOf(fdateString);

                System.debug('convertedDate---->'+convertedDate);

                return convertedDate;
        }else{
            return null;
        }        
    }

}