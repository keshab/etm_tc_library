/*
*Purpose: For formatting the exception occurred & save the logs for further processing it 
*VERSION  	AUTHOR                     DETAIL                                 
*1.0     	Pavithra Gajendra          INITIAL DEVELOPMENT               
*
*/
public class CustomException extends Exception {
	
	//----Attributes
	private static List<Exception_Log__c> exceptionList = new list<Exception_Log__c>();
     
     
    //---- Method that adds exception to the Exception Log Object	
    public static void addException (Exception exptn,String message,  String className, String methodName)
    {	
        Exception_Log__c log = new Exception_Log__c();
        log.Class_Name__c = className ; 
        log.Method_Name__c = methodName ; 
        log.Error_Details__c = exptn.getStackTraceString() ; 
        log.Error_Message__c = exptn.getMessage() ; 
        log.Error_Type__c = exptn.getTypeName() ; 
        log.Line_Number__c = exptn.getLineNumber() ;                           
        exceptionList.add(log); 
		saveExceptionLog();        
    }
    
    //---- Method that adds exception to the Exception Log Object with input	
    public static void addExceptionWithInput(Exception exptn,String message,  String className, String methodName,String input)
    {	
        Exception_Log__c log = new Exception_Log__c();
        log.Class_Name__c = className ; 
        log.Method_Name__c = methodName ; 
        log.Error_Details__c = exptn.getStackTraceString() ; 
        log.Error_Message__c = exptn.getMessage() ; 
        log.Error_Type__c = exptn.getTypeName() ; 
        log.Line_Number__c = exptn.getLineNumber() ;
        log.Input_JSON__c = input;                           
        exceptionList.add(log); 
        saveExceptionLog();        
    }
	
    //---- Method to add expection for services using Database.SaveResult & Database.UpdateResult
    public static void addServicesException (Database.Error error,String message,  String className, String methodName,String input)
    {	
        Exception_Log__c log = new Exception_Log__c();
        log.Class_Name__c = className ; 
        log.Method_Name__c = methodName ;  
        log.Error_Message__c = error.getMessage() ;         
        log.Field_Names__c = String.join(error.getFields(), ',');
        log.Custom_Message__c = message ;
        log.Status_Code__c = String.valueOf(error.getStatusCode());
        log.Input_JSON__c = input; 
        exceptionList.add(log); 
        saveExceptionLog();        
    }
	
	//----- Save method invoked by all various exception
	public static void saveExceptionLog()
    {
        if(!exceptionList.isEmpty())
        {
            Database.insert(exceptionList,false); 
        }
    }
}