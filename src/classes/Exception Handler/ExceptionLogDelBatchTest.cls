@isTest
public class ExceptionLogDelBatchTest {

     static testmethod void test() {
             Exception_Log__c excepRec = new Exception_Log__c();
             excepRec.CurrencyIsoCode = 'INR';
             Insert excepRec;
             
             ExceptionLogDelBatchClass excepLogBatch = new ExceptionLogDelBatchClass();  
             Test.startTest();
             Database.executeBatch(excepLogBatch,1);
             Test.stopTest();                 
     }

}