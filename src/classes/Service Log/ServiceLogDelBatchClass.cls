/*****************************************************************************
@Name:  ServiceLogDelBatchClass 
@=========================================================================
@Purpose: Batch class which deletes the Records of Service_Log__c 
                                                                                                   
@=========================================================================
@History                                                            
@---------                                                            
@VERSION AUTHOR                            DATE                DETAIL                                 
@1.0 -   Sachin Patkar                     10-05-2016       INITIAL DEVELOPMENT               
 ******************************************************************************/
global with sharing class ServiceLogDelBatchClass implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext BC){   
         /*******************************************************************
                Purpose:  start method to return record whose created Date is less then 15 days                                     
                ---------------------------------------------------                               
                VERSION   AUTHOR              DATE            DETAIL                                 
                1.0 -    Sachin Patkar        14/12/2015      INITIAL DEVELOPMENT   
                                
        *********************************************************************/ 
        Integer numOfDays = Integer.ValueOf(System.Label.Service_Log_Days);
        system.debug('numOfDays--'+numOfDays );
        Date dateFilter = date.today().addDays(-numOfDays);   
        String dateValString = string.valueof(dateFilter)+'T00:00:00.000z'; 
        String serviceLogRec = 'SELECT id FROM Service_Log__c WHERE CreatedDate < '+ dateValString ;
        return Database.getQueryLocator(serviceLogRec);        
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> Scope){
          /*******************************************************************
            Purpose:  execute  method to Delete the Records                                     
            ---------------------------------------------------                               
            VERSION    AUTHOR                              DATE            DETAIL                                 
            1.0 -    Sachin Patkar        1/2/2016      INITIAL DEVELOPMENT   
                            
     *********************************************************************/
        List<Service_Log__c> listOfServiceLogRec = new List<Service_Log__c>();
        for(sObject sobj :  Scope ){     
             Service_Log__c serviveLog = new Service_Log__c();
             serviveLog.Id = sobj.Id;
             listOfServiceLogRec.add(serviveLog);
        }  
        system.debug('listOfServiceLogRec ===='+ listOfServiceLogRec.size());
        
        if(listOfServiceLogRec.size() > 0){
           System.debug('***************************************inside the If Condition to DElete'+listOfServiceLogRec.size());
            try{
                 Delete listOfServiceLogRec; 
            }catch(Exception e){
                 System.debug('ERROR:' + e);
            }         
        }   
    }
    
     global void finish(Database.BatchableContext BC){                   
          
    }
}