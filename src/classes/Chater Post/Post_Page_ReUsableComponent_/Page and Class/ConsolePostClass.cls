/*****************************************************************************
@Name:  ConsolePostClass     
@=========================================================================
@Purpose: Class which Creates the FeedItem Record  Under the Record and Sends back the Sucess or failure message                                                                                                 
@=========================================================================
@History                                                            
@---------                                                            
@VERSION      AUTHOR                            DATE                DETAIL                                 
@1.0 -      Sachin Patkar                       25-07-2016         INITIAL DEVELOPMENT
             
 ******************************************************************************/

global class ConsolePostClass{

    @RemoteAction
    global static String savePostComment(String comment, String recordId , String boldTxt, String italics, String underline,String noStyle){
         
		 /*  -------------------    Declaration of Varibles ---------------------   */
		  Case cs = new Case();
          Trip__c trp = new Trip__c();          
          
          FeedItem  fitems= new FeedItem();
          fitems.IsRichText = true;       
          
          String str = comment;
		  
		  /*  ----- Checks which style should be applied while saving ----- */
		  
          if(boldTxt.equals('True')){              
              fitems.Body = '<b>'+ str +'</b>';             
              
          }else if(italics.equals('True')){ 		  
               fitems.Body = '<i>'+ str +'</i>';
			   
          }else if(underline.equals('True')){
               fitems.Body = '<u>'+ str +'</u>';              
          
          }else{
             fitems.Body = comment;
          }  
                    
          fitems.type='TextPost';          
          fitems.parentId = recordId ;
          
           
          try{
		  
		    /*       ------------    Finding Prefix for Case Object(500)     ------------    */
            Schema.DescribeSObjectResult r = Case.sObjectType.getDescribe();  
            String getKeyPrefixGroup = r.getKeyPrefix();
               if(recordId .startsWith(getKeyPrefixGroup)){			   
                   cs  = [SELECT HQ_Trip_Id__c  FROM CASE WHERE Id =: recordId ];                   
               
               }else if(RecursiveClass.isNotesSynced == true){                     
                   trp  = [SELECT Trip_Id__c FROM TRIP__c WHERE Id =: recordId ];    
                       
               }
               
          }catch(Exception e){
               system.debug('Exception -------'+e);  
          }
          
          try{ 
         	  insert fitems;
              return 'Sucess';
          }catch(Exception e){             
               return 'failure';            
          }
    }
}