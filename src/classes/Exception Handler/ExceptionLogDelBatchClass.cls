/*****************************************************************************
@Name:  ExceptionLogDelBatchClass
@=========================================================================
@Purpose: Batch class which deletes the Records of Exception_Log__c
                                                                                                   
@=========================================================================
@History                                                            
@---------                                                            
@VERSION AUTHOR                            DATE                DETAIL                                 
@1.0 -   Sachin Patkar                     10-05-2016       INITIAL DEVELOPMENT               
 ******************************************************************************/
global with sharing class ExceptionLogDelBatchClass implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext BC){   
         /*******************************************************************
                Purpose:  start method to return record Depending on the 
                          Created Date As mentioned in the Custom Label                                    
                ---------------------------------------------------                               
                VERSION   AUTHOR              DATE            DETAIL                                 
                1.0 -    Sachin Patkar        14/12/2015      INITIAL DEVELOPMENT   
                                
        *********************************************************************/ 
        Integer numOfDays = Integer.ValueOf(System.Label.Exception_Log_Days);
        system.debug('numOfDays--'+numOfDays );
        Date dateFilter = date.today().addDays(-numOfDays);   
        String dateValString = string.valueof(dateFilter)+'T00:00:00.000z'; 
        String exceptionLogRec = 'SELECT id FROM Exception_Log__c WHERE CreatedDate < '+ dateValString ;
        return Database.getQueryLocator(exceptionLogRec);        
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> Scope){
          /*******************************************************************
            Purpose:  execute  method to Delete the Records                                     
            ---------------------------------------------------                               
            VERSION AUTHOR                              DATE            DETAIL                                 
            1.0 -Dhriti Krishna Ghosh Moulick         1/2/2016      INITIAL DEVELOPMENT   
                            
     *********************************************************************/
        List<Exception_Log__c> listOfExceptionLogRec = new List<Exception_Log__c>();
        for(sObject sobj :  Scope ){     
             Exception_Log__c serviveLog = new Exception_Log__c();
             serviveLog.Id = sobj.Id;
             listOfExceptionLogRec.add(serviveLog);
        }  
        system.debug('listOfServiceLogRec ===='+ listOfExceptionLogRec.size());
        
        if(listOfExceptionLogRec.size() > 0){
           System.debug('***************************************inside the If Condition to DElete'+listOfExceptionLogRec.size());
            try{
                Delete listOfExceptionLogRec; 
            }catch(Exception e){
                 System.debug('ERROR:' + e);
            }         
        }   
    }
    
     global void finish(Database.BatchableContext BC){                   
          
    }
}